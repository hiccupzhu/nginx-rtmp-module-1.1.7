#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on Oct 14, 2015

@author: szhu
'''

from flask import Flask
from flask.globals import request


app = Flask('flask_test');


@app.route('/api/live/check', methods=['GET', 'POST'])
def index():
    print "     %s"  % (request.path);
    print "     form:", request.form;
    print "     args:", request.args;
    
    if(request.args['token'] == '296662695') :
        return '1';
    else:
        return '0';


###########################################################################################3
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True);