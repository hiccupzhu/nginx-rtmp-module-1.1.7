/*
 * ngn_rtmp_util.c
 *
 *  Created on: Oct 13, 2015
 *      Author: szhu
 */

#include <ngx_core.h>
#include "ngx_rtmp_util.h"

int
ngx_ngxstr_to_char(ngx_str_t *in, char* out)
{
    memcpy(out, in->data, in->len);
    out[in->len] = 0;

    av_println("  out=%s", out);

    return 0;
}


int
ngx_char_to_ngxstr(ngx_str_t *out, u_char* in)
{
    out->len = ngx_strlen(in);
    out->data = (u_char*)strdup((char*)in);

    return 0;
}


void
ngx_str_copy(ngx_pool_t  *pool, ngx_str_t *dst, ngx_str_t *src)
{
    dst->len = src->len;
    dst->data = ngx_pstrdup(pool, src);
}



void
ngx_url_copy(ngx_pool_t  *pool, ngx_url_t *dst, ngx_url_t *src)
{
    *dst = *src;

    ngx_str_copy(pool, &dst->url, &src->url);
    ngx_str_copy(pool, &dst->host, &src->host);
    ngx_str_copy(pool, &dst->port_text, &src->port_text);
    ngx_str_copy(pool, &dst->uri, &src->uri);

    dst->err = NULL;
}


void
ngx_url_free(ngx_pool_t  *pool, ngx_url_t **p)
{
    ngx_str_free(pool, &(*p)->url);
    ngx_str_free(pool, &(*p)->host);
    ngx_str_free(pool, &(*p)->port_text);
    ngx_str_free(pool, &(*p)->uri);

    ngx_pfree(pool, *p);
    *p = NULL;
}


/*
 * split url: http://localhost:8080/test?A=aaa&B=bbb
 * result: ngx_array_t [A -> aaa; B -> bbb]
 */
ngx_array_t *
ngx_url_split(ngx_pool_t *pool, u_char* url)
{
    u_char *p, *end;

    end = url + ngx_strlen(url);
    p = url;

    for(; p < end; p ++){ if(*p == '?') break; }
    if( p == end ) p = url;

    ngx_array_t * params = ngx_array_create(pool, 10, sizeof(ngx_pair_t));
    if(params == NULL) return NULL;

    memset(params->elts, 0, 10*sizeof(ngx_pair_t));

    ngx_pair_t *param = NULL;


    for(; p < end; p ++){
        u_char *pkv;
        if(*p == '&'){
            param = ngx_array_push(params);
            pkv = param->key;
        }else{
            if(param == NULL) {
                param = ngx_array_push(params);
                pkv = param->key;
            }
            if(*p == '='){
                pkv = param->value;
            }else{
                *pkv ++ = *p;
            }
        }
    }

    return params;
}


ngx_array_t *
ngx_url_array_insert(ngx_array_t *params, char* key, char* value)
{
    ngx_pair_t *param = NULL;
    int klen,vlen;

    klen = strlen(key);
    vlen = strlen(value);

    param = ngx_array_push(params);

    memcpy(param->key, key, klen);
    memcpy(param->value, value, vlen);

    param->key[klen] = 0;
    param->value[vlen] = 0;

    return params;
}


ngx_str_t*
ngx_url_compile_with_array(ngx_pool_t *pool, ngx_array_t *params, ngx_str_t *url)
{
    ngx_uint_t i;

    char var_key[64];
    u_char *res;
    const int BUF_MAX = 2048;

    res = ngx_pcalloc(pool, BUF_MAX);

    memcpy(res, url->data, url->len);

    ngx_pair_t *param = params->elts;
    for(i = 0; i < params->nelts; i++, param ++ ){
        memset(var_key, 0, sizeof(var_key));
        sprintf(var_key, "$%s", param->key);

        strrpl((char*)res, var_key, (char*)param->value);
    }

    ngx_pfree(pool, url->data);

    url->data = res;
    url->len = ngx_strlen(res);

    return url;
}

u_char*
memstr(u_char* full_data, int full_data_len, char* substr)
{
    if (full_data == NULL || full_data_len <= 0 || substr == NULL) {
        return NULL;
    }

    if (*substr == '\0') {
        return NULL;
    }

    int sublen = strlen(substr);

    int i;
    u_char* cur = full_data;
    int last_possible = full_data_len - sublen + 1;
    for (i = 0; i < last_possible; i++) {
        if (*cur == *substr) {
            //assert(full_data_len - i >= sublen);
            if (memcmp(cur, substr, sublen) == 0) {
                //found
                return cur;
            }
        }
        cur++;
    }

    return NULL;
}


ngx_buf_t*
ngx_rtmp_get_body(ngx_pool_t *pool, ngx_chain_t *in)
{
    ngx_buf_t* buf;
    ngx_chain_t *b;

    u_char *p = NULL;

    for(b = in; b; b = in->next){
        int blen = b->buf->end - b->buf->start;

        p = (u_char*)memstr(b->buf->start, blen, "\r\n\r\n");
        if(p) break;

    }

    if(!p) return NULL;

    buf = ngx_calloc_buf(pool);

    buf = b->buf;
    buf->shadow = b->buf;
    buf->start = p + 4;

    return buf;
}


int
ngx_rtmp_parse_url(ngx_pool_t  *pool, ngx_url_t *u, ngx_str_t url)
{
    int ret = NGX_OK;

    memset(u, 0, sizeof(ngx_url_t));
    u->default_port = 1935;
    u->uri_part = 1;

    ngx_str_free(pool, &u->url);

    u->url = url;



    if (ngx_strncasecmp(u->url.data, (u_char *) "rtmp://", 7) == 0) {
        u->url.data += 7;
        u->url.len  -= 7;
    }

    if (ngx_parse_url(pool, u) != NGX_OK) {
        if (u->err) {
            av_println("err:%s", u->err);
        }
        ret = NGX_ERROR;
    }


    return ret;
}


/*
 * s: origin string
 * s1: old string
 * s2: new string
 */
char *
strrpl(char *s, const char *s1, const char *s2)
{
    char *ptr;

    while (1) {
        ptr = strstr(s, s1); /* 如果在s中找到s1 */
        if (ptr == NULL) break;

        memmove(ptr + strlen(s2) , ptr + strlen(s1), strlen(ptr) - strlen(s1) + 1);

        memcpy(ptr, &s2[0], strlen(s2));
    }

    return s;
}


int
ngx_str_setval(ngx_pool_t  *pool, ngx_str_t *s, char* name, ngx_str_t s2)
{
    char* buf  = ngx_pcalloc(pool, 1024);
//    char* buf1 = ngx_pcalloc(pool, 256);
    char buf2[256];

    memset(buf2, 0, 256);

    memcpy(buf, s->data, s->len);
    memcpy(buf2, s2.data, s2.len);

    strrpl(buf, name, buf2);
    ngx_pfree(pool, s->data);


    s->len = strlen(buf);
    s->data = (u_char*)buf;

    return 0;
}




