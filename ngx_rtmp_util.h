/*
 * ngx_rtmp_util.h
 *
 *  Created on: Oct 13, 2015
 *      Author: szhu
 */

#ifndef NGX_RTMP_UTIL_H_
#define NGX_RTMP_UTIL_H_

#include "ngx_rtmp.h"

#define PAIR_MAX 64

typedef struct {
    u_char key[PAIR_MAX];
    u_char value[PAIR_MAX];
}ngx_pair_t;



int             ngx_ngxstr_to_char(ngx_str_t *in, char* out);
int             ngx_char_to_ngxstr(ngx_str_t *out, u_char* in);
void            ngx_str_copy(ngx_pool_t  *pool, ngx_str_t *dst, ngx_str_t *src);
void            ngx_url_copy(ngx_pool_t  *pool, ngx_url_t *dst, ngx_url_t *src);
void            ngx_url_free(ngx_pool_t  *pool, ngx_url_t **p);

ngx_array_t *   ngx_url_split(ngx_pool_t *pool, u_char* url);
ngx_array_t *   ngx_url_array_insert(ngx_array_t *params, char* key, char* value);
ngx_str_t *     ngx_url_compile_with_array(ngx_pool_t *pool, ngx_array_t *params, ngx_str_t *url);

u_char *        memstr(u_char* full_data, int full_data_len, char* substr);
ngx_buf_t *     ngx_rtmp_get_body(ngx_pool_t *pool, ngx_chain_t *in);

int             ngx_rtmp_parse_url(ngx_pool_t  *pool, ngx_url_t *u, ngx_str_t url);
char *          strrpl(char *s, const char *s1, const char *s2);
int             ngx_str_setval(ngx_pool_t  *pool, ngx_str_t *s, char* name, ngx_str_t s2);


#ifndef ngx_str_free
#define ngx_str_free(pool, p) do{\
    if((p)->len == 0) break;\
    ngx_pfree(pool, (p)->data);\
    (p)->data = NULL;\
    (p)->len = 0;\
}while(0)
#endif


#ifndef av_println
#define av_println(fmt, ...) \
    do{\
        ngx_log_stderr(0, "[%s:%s:%d]" fmt, __FILE__, __func__, __LINE__, ##__VA_ARGS__);\
    }while(0)
#endif

/*
#ifndef ngx_print
#define ngx_print(fmt, ...) \
    do{\
        ngx_log_stderr(0, "[%s:%d]" fmt, __FILE__, __LINE__, ##__VA_ARGS__);\
    }while(0)
#endif
*/


#endif /* NGX_RTMP_UTIL_H_ */
